package com.ey.wamacademy.capstoneapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("controller")
@SpringBootApplication
public class CapstoneApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CapstoneApiApplication.class, args);
	}

}
