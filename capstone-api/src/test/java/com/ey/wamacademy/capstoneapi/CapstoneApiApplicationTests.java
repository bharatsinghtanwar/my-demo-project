package com.ey.wamacademy.capstoneapi;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@ComponentScan()
class CapstoneApiApplicationTests {

	@Test
	void contextLoads() {
	}

}
